package com.qinb.entity;

public class Nav {
	
	private Integer id;
	private String navName;
	private String navPageCode;
	private String navIcon;
	private String navColor;
	private String content;
	private Integer orderNo;
	
	
	public Integer getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(Integer orderNo) {
		this.orderNo = orderNo;
	}
	public String getNavPageCode() {
		return navPageCode;
	}
	public void setNavPageCode(String navPageCode) {
		this.navPageCode = navPageCode;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNavName() {
		return navName;
	}
	public void setNavName(String navName) {
		this.navName = navName;
	}
	public String getNavIcon() {
		return navIcon;
	}
	public void setNavIcon(String navIcon) {
		this.navIcon = navIcon;
	}
	public String getNavColor() {
		return navColor;
	}
	public void setNavColor(String navColor) {
		this.navColor = navColor;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
}
