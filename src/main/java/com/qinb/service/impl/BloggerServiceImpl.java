package com.qinb.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.qinb.dao.BloggerDao;
import com.qinb.entity.Blogger;
import com.qinb.service.BloggerService;

/**
 * ����Serviceʵ����
 * @author Administrator
 *
 */
@Service("bloggerService")
public class BloggerServiceImpl implements BloggerService{

	@Resource
	private BloggerDao bloggerDao;
	
	public Blogger getByUserName(String userName) {
		return bloggerDao.getByUserName(userName);
	}

	public Blogger find() {
		return bloggerDao.find();
	}

	@Override
	public Integer update(Blogger blogger) {
		// TODO Auto-generated method stub
		return bloggerDao.update(blogger);
	}

}
